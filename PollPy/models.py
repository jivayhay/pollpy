'''
Created on 26 déc. 2014

@author: Jean-Vincent
'''

from django.db import models
from django.db.models import Model
from django.contrib.auth.models import Group, User
from django.core import urlresolvers
from django.core.exceptions import ValidationError
import pdb
    

class Poll(Model):
    name = models.CharField(max_length=64, blank=False)
    description = models.TextField()
    open = models.BooleanField(default=True, blank=False)
    date_created = models.DateTimeField()
    date_closed = models.DateTimeField()
    restrict_group = models.ForeignKey(Group, blank=True, null=True)
    owner = models.ForeignKey(User)
    
    def __str__(self):
        return self.name
    
    def get_poll(self):
        return {'poll': self, 'questions': [{'question': question, 'choices': question.get_choices()} for question in self.get_questions()]}
    
    def get_results(self):
        return {'poll': self, 'questions': [{'question': question, 'results': question.get_results()} for question in self.get_questions()]}
    
    def user_can_vote(self, user):
        return (user.groups.filter(name=self.restrict_group.name).exists() if self.restrict_group != None else True) or user.is_staff
    

class Question(Model):
    poll = models.ForeignKey(Poll)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    max_answers = models.IntegerField()
    required = models.BooleanField(default=True)
    
    def __str__(self):
        return self.title
    
    def edit_link(self):
        if self.id:
            url = urlresolvers.reverse('admin:PollPy_question_change', args=(self.id, ))
            return '<a href="%s" target="_popup">Details</a>' % url
        return 'You need to save before adding choices'
    edit_link.allow_tags = True
    edit_link.short_description = ''
    
    def get_results_detailed(self):
        return [(choice, choice.get_answer_stats()) for choice in self.get_choices()]
    
    def get_results(self):
        return [{'choice': choice, 'result': len(choice.get_answers())} for choice in self.get_choices()]


class Choice(Model):
    question = models.ForeignKey(Question)
    name = models.CharField(max_length=127)
    is_text = models.BooleanField(default=False)
    
    class Meta:
        order_with_respect_to = 'question'
    
    def __str__(self):
        return self.name
    
    def votes(self):
        if self.id:
            return len(self.get_answers())
        return 'n/a'
    votes.allow_tags = True
    
    def get_answer_stats(self):
        return [(len(answ), answ) for answ in self.get_answers()]
    
    def is_in_poll(self, poll):
        return self.question.poll == poll


class Answer(Model):
    question = models.ForeignKey(Question)
    choice = models.ForeignKey(Choice)
    user = models.ForeignKey(User)
    content = models.CharField(max_length=512, blank=True)
    
    class Meta:
        order_with_respect_to = 'choice'
        unique_together = ('question', 'choice', 'user')
        
    def save(self, *args, **kwargs):
        #pdb.set_trace()
        if (self.question.max_answers > 0 and 
                Answer.objects.all().filter(question=self.question, user=self.user).count() >= self.question.max_answers):
            raise ValidationError('Wrong number of answers')
        else:
            super(Answer, self).save(*args, **kwargs)
        
    
    def __str__(self):
        if self.choice.is_text:
            return self.content
        else:
            return (str(self.question) + " "  + str(self.choice))


# Poll methods
def get_questions(self):
    return Question.objects.all().filter(poll=self)

def delete_answers(self, user):
    for q in Question.objects.all().filter(poll=self):
        for answer in Answer.objects.all().filter(question=q):
            answer.delete()

Poll.get_questions = get_questions
Poll.delete_answers = delete_answers

# Question methods
def get_choices(self):
    return Choice.objects.all().filter(question=self)

Question.get_choices = get_choices


# Choice methods
def get_answers(self):
    return Answer.objects.all().filter(choice=self)

Choice.get_answers = get_answers