__all__ = ["home",
           "poll",
           "polls",
           "results",
           "create_poll",
           "edit_poll",
           "user",
           "register",
           ]