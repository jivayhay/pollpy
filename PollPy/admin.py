'''
Created on 26 déc. 2014

@author: Jean-Vincent
'''

from PollPy.models import *
from django.contrib import admin

class QuestionLinkInline(admin.TabularInline):
    model = Question
    readonly_fields = ('edit_link', )
    
class PollAdmin(admin.ModelAdmin):
    fieldset = [
                (None, {'fields': ['name', 'description', 'open',]}),
                ('Date', {'fields': ['date_created', 'date_closed',]}),
                ('Restrictions', {'fields': ['restrict_registered', 'restrict_group',]}),
                ]
    inlines = [QuestionLinkInline]


class ChoicesInline(admin.TabularInline):
    model = Choice
    readonly_fields = ('votes', )

class QuestionAdmin(admin.ModelAdmin):
    inlines = [ChoicesInline]

class ChoiceAdmin(admin.ModelAdmin):
    fieldset = [
                (None, {'fields': ['name', 'is_text', 'votes',]}),
                ]

admin.site.register(Poll, PollAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
#admin.site.register(Answer)